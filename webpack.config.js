const path = require("path");

module.exports = {
  mode: "development",
  devtool: "source-map",
  entry: ["./src/index.tsx"],
  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: "/build/",
    filename: "bundle.js"
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".scss", ".sass"],
    alias: {
      assets: path.resolve(__dirname, "assets")
    }
  },

  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.tsx?$/,
        use: "source-map-loader",
        exclude: /node_modules/
      },
      {
        test: /\.tsx?$/,
        loader: "babel-loader",
        exclude: /node_modules/
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          "style-loader",
          // Translates CSS into CommonJS
          {
            loader: "css-loader",
            options: {
              modules: true
            }
          },
          // Compiles Sass to CSS
          "sass-loader"
        ]
      },
      {
        test: /\.(png|jpg|woff|woff2|eot|ttf|svg)$/,
        loader: "url-loader?limit=100000"
      }
    ]
    /*
     * Define the loaders to be used. Regex will test the type of files on
     * which the loader is to be applied. The excluded files are also mentioned.
     * Loaders are used mainly to transpile the file before bundling.
     */
  },
  devServer: {
    liveReload: true
  }
};
