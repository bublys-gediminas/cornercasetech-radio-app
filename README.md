# Radio application

## Start application
1. run `npm ci`
2. run `npm start`
3. open browser at the following url http://localhost:8080

### Running tests

1. run `npm run test`
