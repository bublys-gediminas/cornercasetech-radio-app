import * as React from "react";
import { Provider } from "react-redux";
import store from "./Redux/store";
import { Radio } from "./Radio/Radio";

import "./styles.scss";
import styles from "./App.scss";

export const MainApp = () => {
  return (
    <Provider store={store}>
      <div className={styles.appContainer}>
        <Radio />
      </div>
    </Provider>
  );
};
