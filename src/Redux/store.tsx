import { applyMiddleware, combineReducers, createStore } from "redux";
import thunkMiddleware from "redux-thunk";
import { radioReducer } from "../Radio/RadioRedux/RadioReduxReducer";

const rootReducer = combineReducers({
  radio: radioReducer
});
export type RootState = ReturnType<typeof rootReducer>;

export default createStore(rootReducer, applyMiddleware(thunkMiddleware));
