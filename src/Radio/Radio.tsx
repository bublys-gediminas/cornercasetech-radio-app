import * as React from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { RadioStationList } from "./RadioStationList/RadioStationList";
import { RadioHeader } from "./RadioHeader/RadioHeader";
import { RadioFooter } from "./RadioFooter/RadioFooter";
import { fetchRadioListAction } from "./RadioRedux/RadioReduxActions";

import styles from "./Radio.scss";

export const Radio = () => {
  const dispatch = useDispatch();

  // Initiate radio stations download
  useEffect(() => {
    dispatch(fetchRadioListAction());
  }, []);

  return (
    <div className={styles.radioWidget}>
      <RadioHeader />
      <RadioStationList />
      <RadioFooter />
    </div>
  );
};
