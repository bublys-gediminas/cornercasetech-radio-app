import configureStore from "redux-mock-store";
import { render, fireEvent } from "@testing-library/react";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import React from "react";
import { RADIO_INITIAL_STATE } from "../RadioRedux/RadioReduxReducer";
import { RadioStationList } from "./RadioStationList";
import { setRadioStationAction } from "../RadioRedux/RadioReduxActions";
import styles from "./RadioStationList.scss";

const mockedStationList = [
  {
    title: "Ballads",
    frequency: 87.1,
    signalType: "FM"
  },
  {
    title: "Maximum",
    frequency: 142.2,
    signalType: "FM"
  }
];

const setup = (radioState?) => {
  const mockedState = {
    radio: {
      ...RADIO_INITIAL_STATE,
      ...radioState
    }
  };
  const mockStore = configureStore([thunk])(mockedState);
  mockStore.dispatch = jest.fn();

  const utils = render(
    <Provider store={mockStore}>
      <RadioStationList />
    </Provider>
  );

  return {
    ...utils,
    store: mockStore
  };
};

describe("RadioStationList", () => {
  it("Should render loading text", () => {
    const utils = setup();

    expect(utils.getByTestId("loading-text")).toBeTruthy();
  });

  it("Should render loading list", () => {
    const utils = setup({
      stationList: mockedStationList
    });

    expect(utils.getAllByTestId("radio-station").length).toBe(2);
  });

  it("Should trigger set active station", () => {
    const utils = setup({
      stationList: mockedStationList
    });

    const stationEntry = utils.getAllByTestId("radio-station")[0];
    fireEvent.click(stationEntry);

    expect(utils.store.dispatch).toHaveBeenCalledWith(
      setRadioStationAction(mockedStationList[0] as any)
    );
  });

  it("Should have active station class", () => {
    const utils = setup({
      stationList: mockedStationList,
      activeStation: mockedStationList[0]
    });
    const stationEntry = utils.getAllByTestId("radio-station")[0];
    expect(stationEntry.querySelector(".visible")).toBeTruthy();
    // stationEntry.querySelector(".visible"));
  });
});
