import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import { setRadioStationAction } from "../RadioRedux/RadioReduxActions";
import { RootState } from "../../Redux/store";
import { RadioStationModel } from "../RadioStationModel";
import classnames from "classnames";

import imageStyles from "assets/styles/images.module.scss";
import radioStyles from "../Radio.scss";
import styles from "./RadioStationList.scss";

export const RadioStationList = () => {
  const { stationList, activeStation } = useSelector(
    (state: RootState) => state.radio
  );

  const dispatch = useDispatch();

  if (!stationList) {
    return (
      <div data-testid="loading-text" className={styles.radioStationsLoading}>
        Loading...
      </div>
    );
  }

  const generateActiveStationClassName = (station: RadioStationModel) =>
    classnames(
      styles.radioStationDetail,
      activeStation === station && styles.visible
    );

  return (
    <div className={styles.radioStationList}>
      {stationList.map((station, index) => (
        <div
          data-testid="radio-station"
          className={styles.radioStation}
          key={index}
          onClick={() => dispatch(setRadioStationAction(station))}
        >
          <div className={generateActiveStationClassName(station)}>
            <button className={radioStyles.radioButton}>
              <span
                className={classnames(imageStyles.img, imageStyles.imageMinus)}
              />
            </button>
            <img className={styles.radioStationCover} />
            <button className={radioStyles.radioButton}>
              <span
                className={classnames(imageStyles.img, imageStyles.imagePlus)}
              />
            </button>
          </div>
          <div className={styles.radioStationTitle}>
            <span>
              {station.title} {station.signalType}
            </span>
            <span className={styles.radioStationFrequency}>
              {station.frequency}
            </span>
          </div>
        </div>
      ))}
    </div>
  );
};
