export interface RadioStationModel {
  frequency: number;
  title: string;
  signalType: "FM" | "AM";
}

export const toRadioStationModel = (data: RadioStationModel) => ({ ...data });
