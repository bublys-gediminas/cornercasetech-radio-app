// TypeScript infers that this function is returning SendMessageAction
import {
  FETCH_RADIO_STATION_LIST_REQUEST,
  FETCH_RADIO_STATION_LIST_SUCCESS,
  RadioActionTypes,
  SET_RADIO_STATION
} from "./RadioReduxTypes";
import { RadioStationModel } from "../RadioStationModel";
import { fetchRadioStationRequest } from "../RadioService";
import { Dispatch } from "redux";

export function setRadioStationAction(
  radioStation: RadioStationModel
): RadioActionTypes {
  return {
    type: SET_RADIO_STATION,
    payload: radioStation
  };
}

export function fetchRadioStationAction(): RadioActionTypes {
  return {
    type: FETCH_RADIO_STATION_LIST_REQUEST
  };
}

export function fetchRadioStationSuccessAction(
  radioStations: RadioStationModel[]
): RadioActionTypes {
  return {
    type: FETCH_RADIO_STATION_LIST_SUCCESS,
    payload: {
      stationList: radioStations
    }
  };
}

// Triggers download a list of radio stations.
export function fetchRadioListAction() {
  return function(dispatch: Dispatch) {
    dispatch(fetchRadioStationAction());

    return fetchRadioStationRequest().then(list =>
      dispatch(fetchRadioStationSuccessAction(list))
    );
  };
}
