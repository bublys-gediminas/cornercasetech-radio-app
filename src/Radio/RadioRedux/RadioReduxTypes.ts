import {RadioStationModel} from '../RadioStationModel';

export const FETCH_RADIO_STATION_LIST_REQUEST = 'FETCH_RADIO_STATION_LIST_REQUEST';

interface FetchRadioStationListRequestAction {
  type: typeof FETCH_RADIO_STATION_LIST_REQUEST
}

export const FETCH_RADIO_STATION_LIST_SUCCESS = 'FETCH_RADIO_STATION_LIST_SUCCESS';

interface FetchRadioStationListSuccessAction {
  type: typeof FETCH_RADIO_STATION_LIST_SUCCESS,
  payload: {
    stationList: RadioStationModel[]
  }
}

export const SET_RADIO_STATION = 'SET_RADIO_STATION';

interface SetRadioStationAction {
  type: typeof SET_RADIO_STATION,
  payload: RadioStationModel
}

export type RadioActionTypes =
  FetchRadioStationListRequestAction
  | FetchRadioStationListSuccessAction
  | SetRadioStationAction

export interface RadioState {
  stationList?: RadioStationModel[],
  activeStation?: RadioStationModel,
  isFetching: boolean,
}
