import {
  FETCH_RADIO_STATION_LIST_REQUEST,
  FETCH_RADIO_STATION_LIST_SUCCESS,
  RadioActionTypes,
  RadioState,
  SET_RADIO_STATION
} from "./RadioReduxTypes";

export const RADIO_INITIAL_STATE: RadioState = {
  activeStation: undefined,
  stationList: undefined,
  isFetching: false
};

export function radioReducer(state = RADIO_INITIAL_STATE, action): RadioState {
  switch ((action as RadioActionTypes).type) {
    case FETCH_RADIO_STATION_LIST_REQUEST:
      return {
        ...state,
        isFetching: true
      };
    case FETCH_RADIO_STATION_LIST_SUCCESS:
      return {
        ...state,
        isFetching: false,
        stationList: action.payload.stationList
      };
    case SET_RADIO_STATION:
      return {
        ...state,
        activeStation: action.payload
      };
    default:
      return state;
  }
}
