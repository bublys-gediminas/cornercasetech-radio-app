import { radioReducer } from "./RadioReduxReducer";
import { RadioState } from "./RadioReduxTypes";
import {
  fetchRadioStationAction,
  fetchRadioStationSuccessAction
} from "./RadioReduxActions";

describe("RadioRedux", () => {
  let initialRadioState: RadioState;

  beforeEach(() => {
    initialRadioState = radioReducer(undefined, {});
  });

  it("Should init default state", () => {
    expect(radioReducer(undefined, {})).toEqual(initialRadioState);
  });

  it("Should toggle to downloading mode", () => {
    const expectedState = {
      ...initialRadioState,
      isFetching: true
    };

    expect(radioReducer(initialRadioState, fetchRadioStationAction())).toEqual(
      expectedState
    );
  });

  it("Should set station list", () => {
    const expectedState = {
      ...initialRadioState,
      stationList: []
    };

    expect(
      radioReducer(initialRadioState, fetchRadioStationSuccessAction([]))
    ).toEqual(expectedState);
  });
});
