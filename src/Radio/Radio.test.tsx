import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { RADIO_INITIAL_STATE } from "./RadioRedux/RadioReduxReducer";
import { Radio } from "./Radio";

const setup = () => {
  const mockStore = configureStore();
  const mockedState = {
    radio: {
      ...RADIO_INITIAL_STATE
    }
  };
  const store = mockStore(mockedState);
  store.dispatch = jest.fn();

  const utils = render(
    <Provider store={store}>
      <Radio />
    </Provider>
  );

  return {
    ...utils,
    store
  };
};

describe("Radio", () => {
  it("Should trigger radio station download when rendered", () => {
    const utils = setup();
    expect(utils.store.dispatch).toHaveBeenCalled();
  });
});
