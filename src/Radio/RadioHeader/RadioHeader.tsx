import * as React from "react";
import imageStyles from "../../../assets/styles/images.module.scss";
import classnames from "classnames";
import styles from "./RadioHeader.scss";
import radioStyles from "../Radio.scss";

export const RadioHeader = () => {
  return (
    <div className={styles.radioHeader}>
      <button className={radioStyles.radioButton}>
        <span
          className={classnames(imageStyles.img, imageStyles.imageBackArrow)}
        />
      </button>

      <span>STATIONS</span>

      <button className={radioStyles.radioButton}>
        <span
          className={classnames(imageStyles.img, imageStyles.imageSwitch)}
        />
      </button>
    </div>
  );
};
