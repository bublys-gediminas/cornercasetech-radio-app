import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { RadioFooter } from "./RadioFooter";
import { toRadioStationModel } from "../RadioStationModel";
import { RADIO_INITIAL_STATE } from "../RadioRedux/RadioReduxReducer";

const setup = (radioState?) => {
  const mockStore = configureStore();
  const mockedState = {
    radio: {
      ...RADIO_INITIAL_STATE,
      ...radioState
    }
  };

  const utils = render(
    <Provider store={mockStore(mockedState)}>
      <RadioFooter />
    </Provider>
  );

  return {
    ...utils
  };
};

describe("RadioFooter", () => {
  it("Should render component without active station", () => {
    const utils = setup();
    expect(utils.queryByTestId("stationTitle")).toBeFalsy();
  });

  it("Should render active station", () => {
    const activeStation = toRadioStationModel({
      title: "Maximum",
      frequency: 142.2,
      signalType: "FM"
    });

    const utils = setup({ activeStation: activeStation });
    expect(utils.getByTestId("stationTitle").textContent).toBe("Maximum FM");
  });
});
