import * as React from "react";
import styles from "./RadioFooter.scss";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";

export const RadioFooter = () => {
  const { activeStation } = useSelector((state: RootState) => state.radio);

  return (
    <div className={styles.radioFooter}>
      {activeStation && (
        <>
          <span className={styles.currentlyPlaying}>CURRENTLY PLAYING</span>
          <span data-testid="stationTitle">
            {activeStation.title} {activeStation.signalType}
          </span>
        </>
      )}
    </div>
  );
};
