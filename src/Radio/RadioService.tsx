import { toRadioStationModel, RadioStationModel } from "./RadioStationModel";

const mockedRadioStationList: RadioStationModel[] = [
  {
    title: "Putin",
    frequency: 66.6,
    signalType: "FM"
  },
  {
    title: "Dribble",
    frequency: 101.2,
    signalType: "FM"
  },
  {
    title: "Doge",
    frequency: 99.4,
    signalType: "FM"
  },
  {
    title: "Ballads",
    frequency: 87.1,
    signalType: "FM"
  },
  {
    title: "Maximum",
    frequency: 142.2,
    signalType: "FM"
  }
];

export function fetchRadioStationRequest(): Promise<RadioStationModel[]> {
  return new Promise<RadioStationModel[]>((resolve, reject) => {
    setTimeout(() => resolve(mockedRadioStationList), 350);
  }).then(list => list.map(toRadioStationModel));
}
